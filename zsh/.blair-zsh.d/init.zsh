# All the directories

export PATH=$HOME/bin:$PATH
export PATH=$HOME/.local/bin:$PATH
export PATH=$PATH:/opt/texlive/2018/bin/x86_64-linux
export PATH=$PATH:/snap/bin

export EDITOR="emacsclient -t"
export VISUAL="emacsclient -t"
export PAGER="less"

export FZF_DEFAULT_COMMAND='ag -g ""'

export GOPATH=$HOME/desk/.src/go
export PATH=$GOPATH/bin:$PATH
export R_LIBS_USER=$HOME/desk/.R

export EMAIL="blairdrummond@protonmail.com"

alias gitlog="git log --pretty=oneline --abbrev-commit"

# kubectl
alias k=kubectl

source <(kubectl completion zsh)

autoload -U colors; colors
source ~/.blair-zsh.d/zsh-kubectl-prompt/kubectl.zsh
RPROMPT='%{$fg[cyan]%}{$ZSH_KUBECTL_CONTEXT}%{$reset_color%}'
