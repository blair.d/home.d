#!/bin/sh

test -n "$LATIN_DB" || LATIN_DB=$HOME/.latin/latin_phrases.db

if [ ! -f "$LATIN_DB" ]; then
    echo "No latin database found. Exiting." >&2
    exit 1
fi

# No SQL injections, thanks.
ARG=$(echo "$@" | grep '^[a-zA-Z0-9,. ][a-zA-Z,. ]*$' | sed 1q)

if test -z "$ARG"; then
    echo "Must provide a valid search term." >&2
    exit 1
fi

sql () {
    cat <<EOF
SELECT printf("Term
====

  %s

Meaning
=======

  %s

Notes
=====

%s", 
latin, 
meaning, 
description
) 
FROM latin_phrases 
WHERE latin="$1" 
COLLATE NOCASE;
EOF
}

sql "$ARG" | 
    sqlite3 "$LATIN_DB" | 
    fold -s -w 80
