#!/bin/bash

BAT=$(acpi -b | grep -E -o '1?[0-9][0-9]?%')


if [ $BAT = "100%" ] || acpi --battery | grep -q Charging; then
    echo "+${BAT}"
else
    echo "-${BAT}"
fi

#[ ${BAT%?} -gt 95  ] && echo "#30B060"
#[ ${BAT%?} -le 5 ]   && echo "#FF8000"
#[ ${BAT%?} -le 20 ]  && echo "#FF8000"
