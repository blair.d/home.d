#!/bin/sh

# the '{ ... } | cat' is just a buffering thing.

{
    curl -Ss 'https://wttr.in?0&T&Q' |
    	cut -c 16- |
    	sed 's/ *$//' |
    	awk '
    BEGIN { printf( "weather: ") }
    { 
        if      (NR <= 2) { printf("%s, ", $0) } 
        else if (NR == 5) { print              } 
    }'
} | cat
